# AltTextBot
Opt-in reminders to add alt text to image attachments.

## Setup
0. Ensure your bot account has Message Content privileged intent enabled

1. Clone repo
```sh
git clone https://gitlab.com/Cynosphere/AltTextBot.git
cd AltTextBot
```

2. Install dependencies
```sh
# If you don't already have pnpm
npm i -g pnpm

pnpm i
```

3. Create `config.json` with the following content:
```json
{
  "token": "bot token here"
}
```

4. Run
```sh
node src/index.js
```

## Data Collection
Only user IDs are stored in the local database when a user runs `/togglestate`. Entries are deleted when toggled off.