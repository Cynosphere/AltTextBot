const Eris = require("eris");
const sqlite3 = require("sqlite3");
const {resolve} = require("path");

const config = require("../config.json");

const database = new sqlite3.Database(resolve(__dirname, "..", "database.db"));
const DatabaseHelpers = require("./database")(database);

const bot = new Eris(config.token, {
  defaultImageFormat: "png",
  defaultImageSize: 1024,
  intents: Eris.Constants.Intents.guilds | Eris.Constants.Intents.guildMessages,
});

bot.on("ready", async () => {
  console.log(
    `Logged in as: ${bot.user.username}#${bot.user.discriminator} (${bot.user.id})`
  );

  const commands = await bot.getCommands();

  let hasCommand = false;
  for (const cmd of commands) {
    if (cmd.name == "togglestate") {
      hasCommand = true;
      break;
    }
  }

  if (!hasCommand) {
    bot.createCommand({
      name: "togglestate",
      description: "Toggle alt text reminder state",
      type: Eris.Constants.ApplicationCommandTypes.CHAT_INPUT,
    });
  }
});

bot.on("interactionCreate", async (interaction) => {
  if (interaction instanceof Eris.CommandInteraction) {
    if (interaction.data.name == "togglestate") {
      const user = interaction.user ?? interaction.member;
      const oldState = await DatabaseHelpers.getUserData(
        user.id,
        "state",
        false
      );
      const newState = !oldState;
      if (newState == false) {
        await DatabaseHelpers.deleteUserData(user.id, "state");
      } else {
        await DatabaseHelpers.setUserData(user.id, "state", newState);
      }

      await interaction.createMessage({
        flags: Eris.Constants.MessageFlags.EPHEMERAL,
        content: `You will ${
          newState ? "now" : "no longer"
        } be notified when your attachments lack accessibility (alt) text.`,
      });
    }
  }
});

bot.on("messageCreate", async (msg) => {
  const remindState = await DatabaseHelpers.getUserData(
    msg.author.id,
    "state",
    false
  );

  if (msg.attachments.length > 0 && remindState == true) {
    const imageAttachments = msg.attachments.filter((x) =>
      x.content_type.startsWith("image/")
    );
    const missingAltText = imageAttachments.filter(
      (x) => x.description == null
    );

    if (missingAltText.length > 0) {
      const channel = await bot.getDMChannel(msg.author.id);
      if (channel) {
        channel.createMessage(
          `${missingAltText.length}/${imageAttachments.length} image attachments in the following message were missing accessibility (alt) text:\n${msg.jumpLink}`
        );
      }
    }
  }
});

bot.connect();
