module.exports = function (database) {
  database.run(
    "CREATE TABLE IF NOT EXISTS user_data (key STRING PRIMARY KEY, value STRING NOT NULL) WITHOUT ROWID"
  );

  function setUserData(id, key, value) {
    return new Promise((resolve, reject) => {
      database.run(
        "REPLACE INTO user_data VALUES ($key,$value)",
        {
          $value: value,
          $key: `${id}[${key}]`,
        },
        (err) => {
          if (err == null) {
            resolve(true);
          } else {
            reject(err);
          }
        }
      );
    });
  }

  function getUserData(id, key, fallback = null) {
    return new Promise((resolve, reject) => {
      database.get(
        "SELECT value FROM user_data WHERE key = $key",
        {
          $key: `${id}[${key}]`,
        },
        (err, row) => {
          if (err == null) {
            resolve(row?.value || fallback);
          } else {
            reject(err);
          }
        }
      );
    });
  }

  function deleteUserData(id, key) {
    return new Promise((resolve, reject) => {
      database.run(
        "DELETE FROM user_data WHERE key = $key",
        {
          $key: `${id}[${key}]`,
        },
        (err) => {
          if (err == null) {
            resolve(true);
          } else {
            reject(err);
          }
        }
      );
    });
  }

  return {
    setUserData,
    getUserData,
    deleteUserData,
  };
};
